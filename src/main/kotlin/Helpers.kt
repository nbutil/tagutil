
inline fun TagDescription(builder: TagDescriptionBuilder.() -> Unit) =
    TagDescriptionBuilder().apply(builder).build()