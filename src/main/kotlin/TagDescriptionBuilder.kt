class TagDescriptionBuilder {

    var tagOpenChar: Char = ' '
    var tagCloseChar: Char = ' '
    var closingTagIdentifier: Char = ' '
    var attributeAssignmentChar: Char = ' '
    var attributeDelimiter: Char = ' '
    var wrapAttribute: Boolean = true
    var attributeWrapChar: Char = ' '

    fun build(): TagDescription =
        TagDescription(
            tagOpenChar = tagOpenChar,
            tagCloseChar = tagCloseChar,
            closingTagIdentifier = closingTagIdentifier,
            attributeAssignmentChar = attributeAssignmentChar,
            attributeDelimiter = attributeDelimiter,
            wrapAttribute = wrapAttribute,
            attributeWrapChar = attributeWrapChar
        )
}