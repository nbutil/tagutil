class TagTypeBuilder() {

    constructor(tagDescription: TagDescription): this() {
        this.tagDescription = tagDescription
    }

    lateinit var tagDescription: TagDescription
    lateinit var tagName: String
    var wrapTag: TagType? = null
    var useOpeningTag: Boolean = true
    var useClosingTag: Boolean = true

    fun build(): TagType =
        TagType(
            tagDescription = tagDescription,
            tagName = tagName,
            useOpeningTag = useOpeningTag,
            useClosingTag = useClosingTag,
            wrapTag = wrapTag
        )
}