/**
 * This can be seen as the "foundation" of a "tag based language" like HTML or BBCode
 *
 * @constructor Creates a [TagDescription]
 *
 * @property [tagOpenChar] The character that represents the opening of a tag declaration e.g. the `<` in `<a href = "www.abc.xyz">`
 * @property [tagCloseChar] The character that represents the closing of a tag declaration e.g. the `>` in `<a href = "www.abc.xyz">
 * @property [closingTagIdentifier] The character that identifies a tag declaration as a closing tag e.g. the `/` in `</a>`
 * @property [attributeAssignmentChar] The character that identifies a value assigned to an attribute e.g. the `=` in `<a href = "www.abc.xyz">`
 * @property [attributeDelimiter] The character that separates attributes e.g. the ` ` between `a` and `href` in `<a href = "www.abc.xyz">`
 * @property [wrapAttribute] Whether or not to wrap the attribute value in [attributeWrapChar]
 * @property [attributeWrapChar] The character that goes around the value of an attribute e.g. the `"` around `www.abc.xyz` in `<a href = "www.abc.xyz">`
 */
data class TagDescription(
    val tagOpenChar: Char,
    val tagCloseChar: Char,
    val closingTagIdentifier: Char,
    val attributeAssignmentChar: Char,
    val attributeDelimiter: Char,
    val wrapAttribute: Boolean,
    val attributeWrapChar: Char
) {

    /**
     * Creates a [TagType] with the given properties.
     * All the `withXXXX` params are by default the ones contained in this [TagDescription]
     *
     * @param [typeName] The name of the [TagType]
     * @param [useOpeningTag] Whether or not to use an opening tag when using the [TagType]
     * @param [useClosingTag] Whether or not to use a closing tag when using the [TagType]
     * @param [wrapTag] A [TagType] used in [TagTextEntry], wraps all given strings unless specified otherwise
     * @param [withTagOpenChar] The character that represents the opening of a tag declaration e.g. the `<` in `<a href = "www.abc.xyz">`
     * @param [withTagCloseChar] The character that represents the closing of a tag declaration e.g. the `>` in `<a href = "www.abc.xyz">
     * @param [withClosingTagIdentifier] The character that identifies a tag declaration as a closing tag e.g. the `/` in `</a>`
     * @param [withAttributeAssignmentChar] The character that identifies a value assigned to an attribute e.g. the `=` in `<a href = "www.abc.xyz">`
     * @param [withAttributeDelimiter] The character that separates attributes e.g. the ` ` between `a` and `href` in `<a href = "www.abc.xyz">`
     * @param [withWrapAttribute] Whether or not to wrap the attribute value in [withAttributeWrapChar]
     * @param [withAttributeWrapChar] The character that goes around the value of an attribute e.g. the `"` around `www.abc.xyz` in `<a href = "www.abc.xyz">`
     *
     * @return [TagType]
     */
    operator fun invoke(
        typeName: String,
        useOpeningTag: Boolean = true,
        useClosingTag: Boolean = true,
        wrapTag: TagType?= null,
        withTagOpenChar: Char = tagOpenChar,
        withTagCloseChar: Char = tagCloseChar,
        withClosingTagIdentifier: Char = closingTagIdentifier,
        withAttributeAssignmentChar: Char = attributeAssignmentChar,
        withAttributeDelimiter: Char = attributeDelimiter,
        withWrapAttribute: Boolean = wrapAttribute,
        withAttributeWrapChar: Char = attributeWrapChar
    ): TagType =
        if (
            withTagOpenChar == tagOpenChar &&
            withTagCloseChar == tagCloseChar &&
            withClosingTagIdentifier == closingTagIdentifier &&
            withAttributeAssignmentChar == attributeAssignmentChar &&
            withAttributeDelimiter == attributeDelimiter &&
            withWrapAttribute == wrapAttribute &&
            withAttributeWrapChar == attributeWrapChar
        ) {
            TagType(this, typeName, useOpeningTag, useClosingTag, wrapTag)
        } else {
            TagType(
                this.copy(
                    tagOpenChar = withTagOpenChar,
                    tagCloseChar = withTagCloseChar,
                    closingTagIdentifier = withClosingTagIdentifier,
                    attributeAssignmentChar = withAttributeAssignmentChar,
                    attributeDelimiter = withAttributeDelimiter,
                    wrapAttribute = withWrapAttribute,
                    attributeWrapChar = withAttributeWrapChar
                ),
                typeName,
                useOpeningTag,
                useClosingTag,
                wrapTag
            )
        }

    /**
     * Creates a [TagType] from the given [builder] lambda
     *
     * @param [builder] The lambda that defines what the properties of the returned [TagType] will be
     *
     * @return [TagType]
     */
    inline operator fun invoke(builder: TagTypeBuilder.() -> Unit): TagType =
        TagTypeBuilder(tagDescription = this).apply(builder).build()

    /** Checks if the [String] is empty or blank */
    private fun String.isEmptyOrBlank(): Boolean = isEmpty() || isBlank()

    /**
     * Wraps [attributeValue] if [wrapAttribute] is `true`
     *
     * @param [attributeValue] The string to potentially wrap
     *
     * @return [String]
     */
    private fun wrapAttributeValue(attributeValue: String): String =
        if (wrapAttribute)
            "$attributeWrapChar$attributeValue$attributeWrapChar"
        else
            attributeValue

    /**
     * Creates a "default" attribute [String] with the given [attributeName] and [attributeValue]
     *
     * @param [attributeName] The name of the attribute
     * @param [attributeValue] The value of the attribute
     *
     * @return [String]
     */
    fun createAttribute(attributeName: String, attributeValue: String): String {
        if (attributeName.isEmptyOrBlank()) {
            if (attributeValue.isEmptyOrBlank()) {
                return ""
            }
            return "$attributeAssignmentChar${wrapAttributeValue(attributeValue)}"
        }
        if (attributeValue.isEmptyOrBlank()) {
            return attributeName
        }
        return "$attributeName$attributeAssignmentChar${wrapAttributeValue(attributeValue)}"
    }

    /**
     * Creates an opening tag with the given [tagName] and [attributes]
     *
     * @param [tagName] The name to put inside the tag
     * @param [attributes] The additional attributes to put inside the tag
     *
     * @return [String]
     */
    fun createOpeningTag(tagName: String, attributes: List<Pair<String, String>>): String {
        val tagString = StringBuilder()
        with(tagString) {
            append(tagOpenChar)
            append(tagName)
            for((attributeName, attributeValue) in attributes) {
                if (!attributeName.isEmptyOrBlank()) {
                    append(attributeDelimiter)
                }
                append(createAttribute(attributeName, attributeValue))
            }
            append(tagCloseChar)
        }
        return tagString.toString()
    }

    /**
     * Creates a closing tag with the given [tagName]
     *
     * @param [tagName] The name to put inside the tag
     *
     * @return [String]
     */
    fun createClosingTag(tagName: String): String = "$tagOpenChar$closingTagIdentifier$tagName$tagCloseChar"

    /**
     * Creates a whole tag with the given [tagName], [attributes] and [wrappedText]
     *
     * @param [tagName] The name to put inside the opening and closing tags (if applicable)
     * @param [useOpeningTag] Whether or not to create an opening tag
     * @param [useClosingTag] Whether or not to create a closing tag
     * @param [wrappedText] The text to put between the opening and closing tags (if applicable)
     * @param [attributes] The attributes to put inside the opening tag (if applicable)
     *
     * @return [String]
     */
    fun createWholeTag(
        tagName: String,
        useOpeningTag: Boolean = true,
        useClosingTag: Boolean = true,
        wrappedText: String = "",
        attributes: List<Pair<String, String>>
    ): String {
        val tagString = StringBuilder()
        with(tagString) {
            if (useOpeningTag) {
                append(createOpeningTag(tagName, attributes))
            }
            append(wrappedText)
            if (useClosingTag) {
                append(createClosingTag(tagName))
            }
        }
        return tagString.toString()
    }
}


