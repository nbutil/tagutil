package example

import TagDescription

val TEXTBOX = TagDescription { wrapAttribute = false }.invoke { tagName = "" }

val BBCODE = TagDescription {
    tagOpenChar = '['
    tagCloseChar = ']'
    closingTagIdentifier = '/'
    attributeAssignmentChar = '='
    attributeDelimiter = ' '
    wrapAttribute = true
    attributeWrapChar = '"'
}

/** Font style */
val font = BBCODE("FONT", withWrapAttribute = false)
val fontSize = BBCODE("SIZE", withWrapAttribute = false)
val fontColor = BBCODE("COLOR")
val boldText = BBCODE("B")
val italicText = BBCODE("I")
val underlineText = BBCODE("U")
val slashText = BBCODE("S")
val subText = BBCODE("SUB")
val superText = BBCODE("SUP")

/** Alignment */
val alignLeft = BBCODE("LEFT")
val alignCenter = BBCODE("CENTER")
val alignRight = BBCODE("RIGHT")
val indent = BBCODE("INDENT")

/** List */
val listItem = BBCODE("*", useClosingTag = false)
val list = BBCODE("LIST", wrapTag = listItem)

/** Code */
val codeBlock = BBCODE("CODE")
val inlineCode = BBCODE("INLINE")

/** Url related */
val textUrl = BBCODE("URL") // These 2 are different for human purposes.
val justUrl = textUrl // It's up to you not to misuse them to confuse yourself!
val image = BBCODE("IMG", withWrapAttribute = false)
val video = BBCODE("VIDEO", withWrapAttribute = false)
val wiki = BBCODE("WIKI", withWrapAttribute = false)
val google = BBCODE("GOOGLE", withWrapAttribute = false)
val user = BBCODE("USER", withWrapAttribute = false)
val email = BBCODE("EMAIL")


/** Misc */
val spoiler = BBCODE("SPOILER")
val quote = BBCODE("QUOTE", withWrapAttribute = false)
val horizontalLine = BBCODE("HR")
val edit = BBCODE("EDIT", withWrapAttribute = false)
