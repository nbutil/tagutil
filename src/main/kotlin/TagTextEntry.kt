class TagTextEntry(initialString: String = "", var value: String = "", var wrapTag: TagType?= null) {
    private val internalStringBuilder = StringBuilder(initialString)
    private fun String.wrap(): String =
        wrapTag.let {
            if (it is TagType) {
                it(this)
            } else {
                this
            }
        }

    operator fun String.unaryPlus() {
        internalStringBuilder.append("\n")
        internalStringBuilder.append(wrap())
    }

    operator fun String.unaryMinus() {
        internalStringBuilder.append(wrap())
    }

    operator fun String.not() {
        internalStringBuilder.append(this)
        internalStringBuilder.append("\n")
    }

    operator fun TagTextEntry.unaryPlus() = +this.toString()
    operator fun TagTextEntry.unaryMinus() = -this.toString()
    operator fun TagTextEntry.not() = !this.toString()

    fun buildWith(tagType: TagType, attributes: List<Pair<String, String>>): String =
        tagType(value = value, withText = internalStringBuilder.append("\n").toString(), attributes = attributes)

    override fun toString(): String = internalStringBuilder.toString()
}