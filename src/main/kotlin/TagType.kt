
/**
 * The most basic components of a "tag language".
 * An HTML tag type example would be the anchor tag, `<a></a>`. That is a type of tag described by the html tag description
 *
 * @constructor Creates a [TagType]
 *
 * @param [tagDescription] The [TagDescription] to base the created strings on
 * @param [tagName] The string to put inside opening and closing tags
 * @param [useOpeningTag] Whether or not to use an opening tag when creating strings
 * @param [useClosingTag] Whether or not to use a closing tag when creating strings
 * @param [wrapTag] The [TagType] to wrap all given strings when inside [TagTextEntry] lambda
 */
data class TagType(
    val tagDescription: TagDescription,
    val tagName: String,
    val useOpeningTag: Boolean = true,
    val useClosingTag: Boolean = true,
    val wrapTag: TagType? = null
) {

    /**
     * Creates a copy of this with [useOpeningTag]
     *
     * @param [useOpeningTag] The new [useOpeningTag] value for the copy
     *
     * @return [TagType]
     */
    fun withOpeningTag(useOpeningTag: Boolean): TagType =
            copy(useOpeningTag = useOpeningTag)

    /**
     * Creates a copy of this with [useClosingTag]
     *
     * @param [useClosingTag] The new [useClosingTag] value for the copy
     *
     * @return [TagType]
     */
    fun withClosingTag(useClosingTag: Boolean): TagType =
            copy(useClosingTag = useClosingTag)

    /**
     * Creates a whole tag with the given params
     *
     * @param [value] The value to assign to the tag name
     * @param [withText] The [String] to put between tags
     * @param [attributes] The attributes to put inside the opening tag
     *
     * @return [String]
     */
    operator fun invoke(value: String, withText: String, attributes: List<Pair<String, String>> = listOf()) =
        tagDescription.createWholeTag(tagName, useOpeningTag, useClosingTag, withText, listOf("" to value)+attributes)

    /**
     * Creates a whole tag with the given params
     *
     * @param [withText] The [String] to put between tags
     * @param [attributes] The attributes to put inside the opening tag
     *
     * @return [String]
     */
    operator fun invoke(withText: String = "", attributes: List<Pair<String, String>> = listOf()) =
        tagDescription.createWholeTag(tagName, useOpeningTag, useClosingTag, withText, attributes)

    /**
     * Builds a [String] from [builder]
     */
    inline operator fun invoke(
        value: String = "",
        withText: String = "",
        attributes: List<Pair<String, String>> = listOf(),
        builder: TagTextEntry.() -> Unit
    ): String =
        TagTextEntry(withText, value, wrapTag).apply(builder).buildWith(this, attributes)
}
